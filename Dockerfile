FROM openjdk:11
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENV PORT 5000
EXPOSE $PORT
CMD [ "sh", "-c", "java -Dserver.port=${PORT} -jar app.jar" ]
