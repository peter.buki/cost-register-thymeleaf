# Cost Register

## Quick run
As a java application: ```mvn package exec:java```
As a containerized application: ```mvn install docker:start```

## Building the app
Run ```mvn install```

## Running the app
After build.

### As a standalone application
Using maven:
```mvn exec:java```

### Running in docker container
With maven:
```mvn docker:start```

With docker compose:
```docker-compose up```

With docker:
```docker run -p 8080:8080 costregister```

### Stopping the docker application
```mvn docker:stop```

## Using the application
If the application was started using java, you can reach it on [localhost](http://localhost:8080).
If it was started in docker, you can reach it on your docker machine IP, e.g. [192.168.99.100:8080](http://192.168.99.100:8080).

### Logging in
You can login using any username / password pair. Your credentials will be saved insecurely. 
If you forgot your password, your account is gone. You will not be able to see other users' costs.

### Adding a cost
After logging in, you need to specify a sum (e.g. ```123.45```) and a date. The date cannot be in the future.
If you are using internet explorer instead of a browser, or some old program from the 90's, 
you need to specify the date manually in ```MM/DD/YYYY``` format.

## Getting the costs
In order to see your costs, a start and end date is required.
You will see the following: the list, the total sum and the average of your costs in the specified time period.


