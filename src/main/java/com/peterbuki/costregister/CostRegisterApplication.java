package com.peterbuki.costregister;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CostRegisterApplication {
    public static void main(String[] args) {
        SpringApplication.run(CostRegisterApplication.class, args);
    }

}
