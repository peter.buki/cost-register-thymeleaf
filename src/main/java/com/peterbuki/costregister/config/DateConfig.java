package com.peterbuki.costregister.config;

import lombok.Getter;
import org.springframework.context.annotation.Configuration;

@Getter
@Configuration
public class DateConfig {

    //NOSONAR
    private Long daysToShow = 30L;
}
