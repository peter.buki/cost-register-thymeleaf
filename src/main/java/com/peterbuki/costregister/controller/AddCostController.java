package com.peterbuki.costregister.controller;

import javax.validation.Valid;
import javax.validation.Validator;
import java.time.LocalDate;

import com.peterbuki.costregister.entity.User;
import com.peterbuki.costregister.form.AddCostForm;
import com.peterbuki.costregister.service.CostService;
import com.peterbuki.costregister.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Slf4j
@Controller
public class AddCostController {
    public static final String ATTRIBUTE_MESSAGE = "message";
    public static final String ATTRIBUTE_FORM = "addCostForm";

    public static final String VIEW_ADD_COST = "addCost";

    public static final String URL_ADD_COST = "/cost/add";

    @Autowired
    private DateUtil dateService;

    @Autowired
    private CostService costService;

    @Autowired
    private Validator validator;

    @GetMapping(URL_ADD_COST)
    public String showAddCostForm(Model model) {
        var addCostForm = new AddCostForm();
        addCostForm.setDate(LocalDate.now());
        model.addAttribute(ATTRIBUTE_FORM, addCostForm);
        return VIEW_ADD_COST;
    }

    @PostMapping(URL_ADD_COST)
    public String addCost(@Valid AddCostForm addCostForm,
                          BindingResult bindingResult,
                          Model model,
                          Authentication authentication) {
        var sum = addCostForm.getSum();
        var date = addCostForm.getDate();
        var user = (User) authentication.getDetails();

        if (bindingResult.hasErrors()) {
            log.info("Form has some errors: {}", addCostForm);
            model.addAttribute(ATTRIBUTE_MESSAGE, String.format("Could not add cost: %s", addCostForm));
        } else {
            log.info("Adding sum {} with date {} for user {}.", sum, date, user);
            costService.addCost(user, addCostForm.getSum(), addCostForm.getDate());
            model.addAttribute(ATTRIBUTE_MESSAGE, String.format("Added sum %s with date of %s", sum, date));
        }
        return VIEW_ADD_COST;
    }

}
