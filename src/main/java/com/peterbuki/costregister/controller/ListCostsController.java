package com.peterbuki.costregister.controller;

import javax.validation.Valid;
import javax.validation.Validator;

import java.time.LocalDate;

import com.peterbuki.costregister.entity.User;
import com.peterbuki.costregister.form.ListCostsForm;
import com.peterbuki.costregister.service.CostService;
import com.peterbuki.costregister.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Slf4j
@Controller
public class ListCostsController {
    public static final String ATTRIBUTE_MESSAGE = "message";
    public static final String ATTRIBUTE_SUM = "sum";
    public static final String ATTRIBUTE_COSTS = "costs";
    public static final String ATTRIBUTE_AVERAGE = "average";
    public static final String VIEW_LIST_COSTS = "listCosts";
    public static final String URL_LIST_COSTS = "/cost/list";
    private static final String ATTRIBUTE_FORM = "listCostsForm";

    @Autowired
    private DateUtil dateService;

    @Autowired
    private CostService costService;

    @Autowired
    private Validator validator;

    @GetMapping(URL_LIST_COSTS)
    public String showCostByDateForm(Model model) {
        var startDate = dateService.getDefaultStartDate();
        var endDate = LocalDate.now();
        var form = new ListCostsForm();
        form.setStartDate(startDate);
        form.setEndDate(endDate);

        model.addAttribute(ATTRIBUTE_FORM, form);
        return VIEW_LIST_COSTS;
    }

    @PostMapping(URL_LIST_COSTS)
    public String listByDate(@Valid ListCostsForm listCostsForm,
                             BindingResult bindingResult,
                             Model model,
                             Authentication authentication) {
        var startDate = listCostsForm.getStartDate();
        var endDate = listCostsForm.getEndDate();
        log.info("Trying to list costs between {} - {} for user {}.",
                startDate, endDate, authentication);
        if (bindingResult.hasErrors()) {
            model.addAttribute(ATTRIBUTE_MESSAGE, "Please fix errors stated below!");
        } else {
            var user = (User) authentication.getDetails();
            var costs = costService.getCostsByDate(user, startDate, endDate);
            var sum = costService.getSumOfCosts(costs);
            var average = costService.getAverageOfCosts(costs, startDate, endDate);

            model.addAttribute(ATTRIBUTE_COSTS, costs);
            model.addAttribute(ATTRIBUTE_SUM, sum);
            model.addAttribute(ATTRIBUTE_AVERAGE, average);
            model.addAttribute(ATTRIBUTE_MESSAGE, String.format("Showing costs between %s - %s", startDate, endDate));
        }
        return VIEW_LIST_COSTS;
    }

}
