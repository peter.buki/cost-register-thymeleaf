package com.peterbuki.costregister.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.Valid;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Positive;
import java.time.LocalDate;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
public class Cost {

    @Id
    @GeneratedValue
    private Long id;

    @Valid
    @ManyToOne
    private User owner;

    @Column
    @Positive
    private Double sum;

    @Column
    @PastOrPresent
    private LocalDate date;

    public Cost(User owner, Double sum, LocalDate date) {
        this.owner = owner;
        this.sum = sum;
        this.date = date;
    }
}
