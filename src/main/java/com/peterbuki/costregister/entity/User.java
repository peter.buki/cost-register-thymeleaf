package com.peterbuki.costregister.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.PastOrPresent;
import java.time.LocalDateTime;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true)
    @NotEmpty
    private String username;

    @Column
    @NotEmpty
    @EqualsAndHashCode.Exclude
    private String password;

    @Column
    @PastOrPresent
    @EqualsAndHashCode.Exclude
    private LocalDateTime lastLogin;

    public User(String username, String password, LocalDateTime lastLogin) {
        this.username = username;
        this.password = password;
        this.lastLogin = lastLogin;
    }
}
