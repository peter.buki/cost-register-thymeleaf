package com.peterbuki.costregister.form;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Positive;
import java.time.LocalDate;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

@Getter
@Setter
@ToString
public class AddCostForm {
    @NotNull(message = "The sum must not be empty!")
    @Positive(message = "The sum must be positive")
    private Double sum;

    @NotNull(message = "Date must not be empty!")
    @PastOrPresent(message = "Date must not be in the future!")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate date;
}
