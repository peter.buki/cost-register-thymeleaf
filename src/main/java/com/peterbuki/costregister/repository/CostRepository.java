package com.peterbuki.costregister.repository;

import java.time.LocalDate;

import com.peterbuki.costregister.entity.Cost;
import com.peterbuki.costregister.entity.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface CostRepository extends CrudRepository<Cost, Long> {

    @Query("SELECT c FROM Cost c WHERE c.owner = :owner AND c.date BETWEEN :startDate AND :endDate ORDER BY c.date DESC")
    Cost[] findAllByUserAndDate(
            @Param("owner") User owner,
            @Param("startDate") LocalDate startDate,
            @Param("endDate") LocalDate endDate);
}
