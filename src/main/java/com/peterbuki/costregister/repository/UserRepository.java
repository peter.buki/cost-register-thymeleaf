package com.peterbuki.costregister.repository;

import com.peterbuki.costregister.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {

    User findByUsername(String username);
}
