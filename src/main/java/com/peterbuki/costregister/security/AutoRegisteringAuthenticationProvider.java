package com.peterbuki.costregister.security;

import java.util.List;

import com.peterbuki.costregister.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class AutoRegisteringAuthenticationProvider implements AuthenticationProvider {

    @Autowired
    private UserService userService;

    @Override
    public Authentication authenticate(Authentication authentication) {
        log.info("Authenticating user: {}", authentication.getPrincipal());
        var principal = authentication.getPrincipal();
        var credentials = authentication.getCredentials();
        var user = userService.authenticateUser(principal, credentials);
        if (user.isPresent()) {
            log.info("User {} logged in.", authentication.getPrincipal());
            var authenticationWithDetails = new UsernamePasswordAuthenticationToken(
                    authentication.getPrincipal(), authentication.getCredentials(), List.of(new SimpleGrantedAuthority("USER")));
            authenticationWithDetails.setDetails(user.get());
            return authenticationWithDetails;
        }
        throw new BadCredentialsException("Invalid user");
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(UsernamePasswordAuthenticationToken.class);
    }
}
