package com.peterbuki.costregister.service;

import javax.validation.ValidationException;
import javax.validation.Validator;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;

import com.peterbuki.costregister.entity.Cost;
import com.peterbuki.costregister.entity.User;
import com.peterbuki.costregister.repository.CostRepository;
import com.peterbuki.costregister.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class CostService {
    @Autowired
    private DateUtil dateUtil;

    @Autowired
    private Validator validator;

    @Autowired
    private CostRepository costRepository;

    public Cost addCost(@NonNull User user,
                        @NonNull Double sum,
                        @NonNull LocalDate date) {
        log.info("Adding sum {} for {} for user {}.", sum, date, user);
        Cost cost = new Cost(user, sum, date);

        return addCost(cost);
    }

    public Cost addCost(@NonNull Cost cost) {
        var violations = validator.validate(cost);
        violations.forEach(e -> log.warn("Violation error for {}: {}", e.getPropertyPath(), e.getMessage()));
        if (!violations.isEmpty()) {
            throw new ValidationException(String.format("There were %d violation errors.", violations.size()));
        }
        return costRepository.save(cost);
    }

    @NotNull
    public Cost[] getCostsByDate(@NonNull User user,
                                 @NonNull LocalDate startDate,
                                 @NonNull LocalDate endDate) {
        log.debug("Fetching costs for user {} for dates between {} - {}", user, startDate, endDate);
        if (endDate.isBefore(startDate)) {
            return costRepository.findAllByUserAndDate(user, endDate, startDate);
        } else {
            return costRepository.findAllByUserAndDate(user, startDate, endDate);
        }
    }

    public Double getSumOfCosts(@NonNull Cost[] costs) {
        return Arrays.stream(costs).map(Cost::getSum).reduce(Double::sum).orElse(0.0);
    }

    public Double getAverageOfCosts(@NonNull Cost[] costs) {
        if (costs.length == 0) {
            return 0.0;
        }
        Double sum = getSumOfCosts(costs);
        Long daysBetween = countNumberOfDays(costs);
        return sum / daysBetween;
    }

    public Double getAverageOfCosts(@NonNull Cost[] costs,
                                    @NonNull LocalDate dateFrom,
                                    @NonNull LocalDate dateTo) {
        if (costs.length == 0) {
            return 0.0;
        }
        Double sum = getSumOfCosts(costs);
        Long daysBetween = Math.abs(ChronoUnit.DAYS.between(dateFrom, dateTo)) + 1;
        return sum / daysBetween;
    }

    private Long countNumberOfDays(Cost[] costs) {
        var earliestDate = costs[0].getDate();
        var latestDate = costs[0].getDate();

        for (Cost cost : costs) {
            if (cost.getDate().isBefore(earliestDate)) {
                earliestDate = cost.getDate();
            }
            if (cost.getDate().isAfter(latestDate)) {
                latestDate = cost.getDate();
            }
        }
        return ChronoUnit.DAYS.between(earliestDate, latestDate) + 1;
    }
}
