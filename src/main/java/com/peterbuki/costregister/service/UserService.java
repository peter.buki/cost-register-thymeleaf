package com.peterbuki.costregister.service;

import java.time.LocalDateTime;
import java.util.Optional;

import com.peterbuki.costregister.entity.User;
import com.peterbuki.costregister.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public Optional<User> authenticateUser(Object principal, Object credentials) {
        var user = userRepository.findByUsername((String) principal);
        if (user == null) {
            log.info("Creating user: {}", principal);
            return Optional.of(createUser(principal, credentials));
        } else if (user.getPassword().equals(credentials)) {
            log.info("User already exists.");
            updateUserLastLogin(user);
            return Optional.of(user);
        } else {
            return Optional.empty();
        }
    }

    private User createUser(Object principal, Object credentials) {
        var newUser = new User((String) principal, (String) credentials, LocalDateTime.now());
        userRepository.save(newUser);
        return newUser;
    }

    private void updateUserLastLogin(User user) {
        user.setLastLogin(LocalDateTime.now());
        userRepository.save(user);
    }
}
