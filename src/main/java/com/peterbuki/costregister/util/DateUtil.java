package com.peterbuki.costregister.util;

import java.time.LocalDate;

import com.peterbuki.costregister.config.DateConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DateUtil {
    private final Long daysToShow;

    @Autowired
    public DateUtil(DateConfig dateConfig) {
        daysToShow = dateConfig.getDaysToShow();
    }

    public LocalDate getDefaultStartDate() {
        return LocalDate.now().minusDays(daysToShow);
    }
}
