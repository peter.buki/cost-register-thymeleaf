package com.peterbuki.costregister;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrlPattern;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public class AuthenticationTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void loginWithValidUserThenAuthenticated() throws Exception {
        SecurityMockMvcRequestBuilders.FormLoginRequestBuilder login = formLogin()
                .user("user")
                .password("password");

        mockMvc.perform(login)
                .andExpect(authenticated().withUsername("user"));
    }


    @Test
    public void loginWithInvalidPasswordThenUnauthenticated() throws Exception {
        SecurityMockMvcRequestBuilders.FormLoginRequestBuilder login = formLogin()
                .user("user1")
                .password("password");

        SecurityMockMvcRequestBuilders.FormLoginRequestBuilder invalidLogin = formLogin()
                .user("user1")
                .password("invalid");

        mockMvc.perform(login)
                .andExpect(authenticated().withUsername("user1"));
        mockMvc.perform(invalidLogin)
                .andExpect(unauthenticated());
    }

    @Test
    public void loginAndLogout() throws Exception {
        SecurityMockMvcRequestBuilders.FormLoginRequestBuilder login = formLogin()
                .user("user1")
                .password("password");

        SecurityMockMvcRequestBuilders.FormLoginRequestBuilder invalidLogin = formLogin()
                .user("user1")
                .password("invalid");

        mockMvc.perform(login)
                .andExpect(authenticated().withUsername("user1"));
        mockMvc.perform(invalidLogin)
                .andExpect(unauthenticated());
    }

    @Test
    public void accessUnsecuredResourceThenOk() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrlPattern("**/login"));
    }

    @Test
    public void accessSecuredResourceUnauthenticatedThenRedirectsToLogin() throws Exception {
        mockMvc.perform(get("/hello"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrlPattern("**/login"));
    }

    @Test
    @WithMockUser
    public void accessSecuredResourceAuthenticatedThenOk() throws Exception {
        mockMvc.perform(get("/cost/add"))
                .andExpect(status().isOk());
        mockMvc.perform(get("/cost/list"))
                .andExpect(status().isOk());
    }
}
