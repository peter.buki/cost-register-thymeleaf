package com.peterbuki.costregister;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

import com.peterbuki.costregister.entity.Cost;
import com.peterbuki.costregister.entity.User;
import com.peterbuki.costregister.repository.CostRepository;
import com.peterbuki.costregister.repository.UserRepository;
import com.peterbuki.costregister.service.CostService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public class CostRegisterApplicationTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CostRepository costRepository;

    @Autowired
    private CostService costService;

    private User user1;
    private User user2;


    @Test
    public void contextLoads() {
    }

    @BeforeEach
    public void setupUsers() {
        user1 = new User(UUID.randomUUID().toString(), "password", LocalDateTime.now());
        user1 = userRepository.save(user1);

        user2 = new User(UUID.randomUUID().toString(), "password", LocalDateTime.now());
        user2 = userRepository.save(user2);
    }

    @AfterEach
    public void removeUsers() {
        costRepository.deleteAll();
        userRepository.deleteAll();
    }

    // The one where user can see own costs only
    @Test
    public void listCosts_TwoUsersFetchCosts_returnsOnlyOwnCosts() {
        var today = LocalDate.now();
        var yesterday = today.minusDays(1);
        var cost1 = new Cost(user1, 1.0, yesterday);
        var cost2 = new Cost(user2, 10.0, today);

        cost1 = costService.addCost(cost1.getOwner(), cost1.getSum(), cost1.getDate());
        cost2 = costService.addCost(cost2.getOwner(), cost2.getSum(), cost2.getDate());

        var costsOfUser1 = costService.getCostsByDate(user1, yesterday, today);
        var costsOfUser2 = costService.getCostsByDate(user2, yesterday, today);

        assertEquals(costsOfUser1.length, 1, "Too many results!");
        assertEquals(costsOfUser2.length, 1, "Too many results!");

        assertEquals(costsOfUser1[0], cost1, "Different cost is returned!");
        assertEquals(costsOfUser2[0], cost2, "Different cost is returned!");
    }

    // The one where list of costs is requested with later date first
    @Test
    public void listCosts_fetchedLaterDateFirst_returnsAll() {
        var day = LocalDate.now().minusDays(30);
        var dayBefore = day.minusDays(1);
        var dayAfter = day.plusDays(1);

        var cost1 = new Cost(user1, 1.0, dayBefore);
        var cost2 = new Cost(user1, 10.0, day);
        var cost3 = new Cost(user1, 100.0, dayAfter);

        cost1 = costService.addCost(cost1);
        cost2 = costService.addCost(cost2);
        cost3 = costService.addCost(cost3);

        var costsOfUser1 = costService.getCostsByDate(user1, day, dayBefore);

        assertEquals(costsOfUser1.length, 2, "Different number of objects!");
        assertEquals(costsOfUser1[0], cost2, "Different cost is returned!");
        assertEquals(costsOfUser1[1], cost1, "Different cost is returned!");
    }
}
