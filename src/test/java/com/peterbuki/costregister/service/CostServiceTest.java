package com.peterbuki.costregister.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import javax.validation.ValidationException;
import java.time.LocalDate;
import java.time.LocalDateTime;

import com.peterbuki.costregister.entity.Cost;
import com.peterbuki.costregister.entity.User;
import com.peterbuki.costregister.repository.CostRepository;
import com.peterbuki.costregister.util.DateUtil;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class CostServiceTest {
    private static final int MAX_DAYS = 1000;

    @Mock
    private CostRepository costRepository;

    @Mock
    private DateUtil dateUtil;

    @Autowired
    @InjectMocks
    private CostService costService;

    @Captor
    private ArgumentCaptor<Cost> costArgumentCaptor;

    private User user;
    private Double sum = 1.0;
    private LocalDate date = LocalDate.now().minusDays(30);

    @BeforeEach
    public void setup() {
        this.user = new User("username", "password", LocalDateTime.now());
    }

    // The one where cost is added
    @Test
    public void testAddCostProperValues_costIsAdded() {
        var cost = new Cost(user, sum, date);

        costService.addCost(user, sum, date);

        verify(costRepository).save(costArgumentCaptor.capture());
        var costSaved = costArgumentCaptor.getValue();
        assertEquals(cost, costSaved);
    }

    // The one where cost with negative sum is added and throws ValidationException
    @Test
    public void testAddCostWithNegativeSum_throwsValidationException() {
        sum = -1.0;

        assertThrows(ValidationException.class, () -> costService.addCost(user, sum, date));

        verify(costRepository, never()).save(any(Cost.class));
    }

    // The one where cost with future date is added and throws ValidationException
    @Test
    public void testAddCostWithFutureDate_throwsValidationException() {
        date = LocalDate.now().plusDays(1);

        assertThrows(ValidationException.class, () -> costService.addCost(user, sum, date));

        verify(costRepository, never()).save(any(Cost.class));
    }

    // The one where list of costs is requested with proper dates
    @Test
    public void testGetCostsByDate_costsReturned() {
        Cost cost1 = new Cost(user, 1.0, date);
        Cost cost2 = new Cost(user, 2.0, date.plusDays(1));
        when(costRepository.findAllByUserAndDate(any(User.class), any(LocalDate.class), any(LocalDate.class)))
                .thenReturn(new Cost[]{cost1, cost2});

        var costs = costService.getCostsByDate(user, date, date);

        verify(costRepository).findAllByUserAndDate(eq(user), eq(date), eq(date));
        assertEquals(2, costs.length, "Should return 2 objects");
        assertEquals(costs[0], cost1, "First cost is not equal.");
        assertEquals(costs[1], cost2, "Second cost is not equal.");
    }

    // The one where list of costs is requested but repository finds nothing
    @Test
    public void testGetCostByDateFindsNoRecords_returnsEmptyResult() {
        when(costRepository.findAllByUserAndDate(any(User.class), any(LocalDate.class), any(LocalDate.class)))
                .thenReturn(new Cost[]{});

        Cost[] costs = costService.getCostsByDate(user, date, date);

        assertEquals(0, costs.length, "Result is not empty");
    }

    // The one where sum is requested
    @Test
    public void testGetCostsSum_returnsSum() {
        Cost cost1 = new Cost(user, 1.0, date);
        Cost cost2 = new Cost(user, 2.0, date.plusDays(1));
        var costs = new Cost[]{cost1, cost2};

        Double sum = costService.getSumOfCosts(costs);

        assertEquals(3.0, sum, "Sum is different.");
    }

    // The one where sum is requested for empty array
    @Test
    public void testGetCostsSumForEmpty_returnsZero() {
        var costs = new Cost[]{};

        Double sum = costService.getSumOfCosts(costs);

        assertEquals(0.0, sum, "Sum is different.");
    }

    // The one where average is requested
    @Test
    public void testGetCostsAverage_returnsAverage() {
        Cost cost1 = new Cost(user, 22.0, date.minusDays(1));
        Cost cost2 = new Cost(user, 33.0, date.plusDays(3));

        Double average = costService.getAverageOfCosts(new Cost[]{cost1, cost2}, cost1.getDate(), cost2.getDate());

        assertEquals(11.0, average, "Average is different.");
    }

    // The one where average is requested for empty array
    @Test
    public void testGetCostsAverageForEmpty_returnsZero() {
        // GIVEN
        Cost[] costs = {};

        // WHEN
        Double average = costService.getAverageOfCosts(costs);

        // THEN
        assertEquals(0.0, average, "Average is not zero for empty array.");
    }

}